let character = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}

console.log(character);
console.log("Result of dot notation:");
console.log(character.name);
console.log("Result of square bracket notation:");
console.log(character["pokemon"]);
console.log("Result of talk method:");
character.talk();


function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;


	//Method
	//"target" parameter represents another pokemon object
	this.tackled = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health -= this.attack;
		console.log("targetPokemon's health is now reduced to " + target.health);
	}
	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackled(pikachu);

console.log(pikachu);

mewtwo.tackled(geodude);

geodude.faint();

console.log(geodude);